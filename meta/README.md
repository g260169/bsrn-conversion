BSRN meta data
========

BSRN meta data has been downloaded January, 9th from https://bsrn.awi.de/stations/listings/

- `bsrn_parameters.tab`
  - Downloaded from https://www.pangaea.de/ddi?request=bsrn/BSRNParameterInUse
- `bsrn_staffs.tab`
  - Downloaded from https://www.pangaea.de/ddi?request=bsrn/BSRNStaff
- `bsrn_stations.tab`
  - Downloaded from https://www.pangaea.de/ddi?request=bsrn/BSRNEvent
